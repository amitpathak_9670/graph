#include<iostream>
#include<unordered_set>
#include<unordered_map>
#include<vector>
#include<stack>
using namespace std;


class graph {
public :
  unordered_map<int,vector<pair<int,int> > > g;
  unordered_set<int> vertices;
  int source_graph1;
  unordered_map<int,vector<pair<int,int> > > g_t;
  unordered_set<int> vertices_t;
  int source_graph2;
  
  void print_graph2();
  void initialize_graph1();
  void print_graph1();
  void topo_util(int source,stack<int> &s,unordered_map<int,int> &color, unordered_map<int,int> &parent);  
  void topologicalSort(int source);
  void depthFirstTraversal(int source);
  
  void initialize_graph2();
  void dfs_util(int source,unordered_map<int,int> &color,unordered_map<int,int> &parent);
};


void graph::initialize_graph1() {
   source_graph1 = 4;
   
   vertices.insert(1);
   vertices.insert(3);
   vertices.insert(4);
   vertices.insert(5);
   vertices.insert(9);

  //First Way
  g[3].push_back(make_pair(1,10));
  g[4].push_back(make_pair(5,3));
  g[4].push_back(make_pair(9,1));
  g[5].push_back(make_pair(1,5));
  g[5].push_back(make_pair(3,1));
  
  //Second Way
  vector<pair<int,int> >edges;
  edges.push_back(make_pair(5,2));
  g.insert(make_pair(9,edges));
}


void graph::initialize_graph2() {
   source_graph2 = 2;
  
   vertices_t.insert(1);
   vertices_t.insert(2);
   vertices_t.insert(3);
   vertices_t.insert(4);
   vertices_t.insert(5);
   vertices_t.insert(6);
   vertices_t.insert(7);
   vertices_t.insert(8);
   g_t[2].push_back(make_pair(1,1));
   g_t[2].push_back(make_pair(3,1));
   g_t[2].push_back(make_pair(4,1));
   g_t[4].push_back(make_pair(5,1));
   g_t[4].push_back(make_pair(6,1));
   g_t[6].push_back(make_pair(8,1));
   g_t[5].push_back(make_pair(7,1));
   g_t[6].push_back(make_pair(7,1));


}

void graph::print_graph1() {
  for(auto it = vertices.begin(); it != vertices.end(); it++) {
     cout<<*it; 
     for(auto eit = g[*it].begin(); eit != g[*it].end(); eit++) {
        cout<<" -> "<<eit->first<<","<<eit->second;
      }
      cout<<endl;
  }
}

void graph::print_graph2() {
  for(auto it = vertices_t.begin(); it != vertices_t.end(); it++) {
     cout<<*it;
     for(auto eit = g_t[*it].begin(); eit != g_t[*it].end(); eit++) {
        cout<<" -> "<<eit->first<<","<<eit->second;
      }
      cout<<endl;
  }
}


void graph::depthFirstTraversal(int source) {
    
    unordered_map<int,int> color;
    //-1 unexplored - white
    //0  explored but not completed - gray
    //1  completed - black
    
    unordered_map<int,int> parent;
    //unordered_map<int,int> distance;
    
    for(auto it=vertices.begin(); it!=vertices.end(); it++) {
        color[*it] = -1;
        parent[*it] = -1;
    }
    
    //Starting DFS
    color[source] = 0;
    dfs_util(source,color,parent);
}

void graph::dfs_util(int source,
                     unordered_map<int,int> &color,
                     unordered_map<int,int> &parent) {
    cout<<source<<" ";
    
    for(auto it=g[source].begin(); it != g[source].end(); it++) {
        if(color[it->first] == -1) {
            color[it->first] = 0;
            parent[it->first] = source;
            dfs_util(it->first,color,parent);
        }
    }
    
    color[source] = 1;
}


void graph::topologicalSort(int source) {

      //Just keeping parent entries
      unordered_map<int,int> parent;

      //For checking whether node is visited , or in progress , or still unvisited
      //-1 unexplored - white  - unvisited
      //0  explored but not completed - gray - in progress (means some children are yet to be processed)
      //1  completed - black  // Node as well as all children are processed
      unordered_map<int,int> color;
     
     //Initializing Values
     for(auto it=vertices_t.begin(); it!=vertices_t.end(); it++) {
        parent[*it] = -1;
        color[*it] = -1;
     }

    //Starting DFS
    stack<int> s;            //This stack will contain topological order 
    color[source] = 0;       //Marking source as in progress

    //Starting TOPO TRAVERSAL with Root 
    topo_util(source,s,color,parent);

    //Printing TOPO TRAVERSAL
    while(!s.empty()) {
        cout<<s.top()<<endl;
        s.pop();
    }
}

void graph::topo_util(int source,
                     stack<int> &s,
                     unordered_map<int,int> &color,
                     unordered_map<int,int> &parent) {
    //Looping over all the children (connections)
    for(auto it=g_t[source].begin(); it != g_t[source].end(); it++) {
            //We have to process unexplored/unvisited nodes only
            if(color[it->first] == -1)    { 
              color[it->first] = 0;
              parent[it->first] = source;
              topo_util(it->first,s,color,parent);
         }
    }
    
    //Marking Node as complete
    color[source] = 1;
    //Putting complete Node into Stack
    s.push(source);
}


int main() {
   graph g1;
   g1.initialize_graph1();
   g1.print_graph1();
   g1.depthFirstTraversal(g1.source_graph1);

   g1.initialize_graph2();
   g1.print_graph2();
   cout<<endl<<endl;
   g1.topologicalSort(g1.source_graph2);

} 
